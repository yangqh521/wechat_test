package com.yqh.wechat.dao;

import org.apache.ibatis.annotations.Mapper;

import com.yqh.wechat.entity.RedPacketSendRecord;

@Mapper
public interface RedPacketSendRecordMapper {
	void createRedPacketSendRecord(RedPacketSendRecord redPacketSendRecord);
}