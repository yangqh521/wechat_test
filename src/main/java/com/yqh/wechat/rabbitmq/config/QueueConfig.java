package com.yqh.wechat.rabbitmq.config;


import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 队列配置
 * @author Yang.Qinghui
 */
@Configuration
public class QueueConfig {
	
	@Bean
	public Queue queueA() {
		
		/**
		 * durable="true" 持久化 rabbitmq重启的时候不需要创建新的队列
		 * auto-delete 表示消息队列没有在使用时将被自动删除 默认是false
		 * exclusive  表示该消息队列是否只在当前connection生效,默认是false
		 */
		return new Queue(RabbitMqConfig.QUEUE_A,true,false,false);
	}
 
    @Bean
    public Queue queueB() {
        return new Queue(RabbitMqConfig.QUEUE_B,true,false,false);
    }



}
