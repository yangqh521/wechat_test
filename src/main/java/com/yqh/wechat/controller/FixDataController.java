package com.yqh.wechat.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.yqh.wechat.dao.RedPacketSendRecordMapper;
import com.yqh.wechat.entity.RedPacketSendRecord;
import com.yqh.wechat.entity.WeChatUserInfo;
import com.yqh.wechat.util.XMLUtil;

@Controller
@RequestMapping("/fix")
public class FixDataController {
	
	private static final SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	static{
		DF.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		SDF.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
	}
	
	@Autowired
	RedPacketSendRecordMapper redPacketSendRecordMapper;
	
	private static String path_name = "C:\\download\\exception_log.txt";
	
	
	@RequestMapping("/fix1")
	public void fix1() throws ParseException{
		try (FileReader reader = new FileReader(path_name);
				BufferedReader br = new BufferedReader(reader)
        ) {
            String line;
            while ((line = br.readLine()) != null) {
            	dealWithLog(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	private void dealWithLog(String line) throws ParseException{
		
		System.out.println("------>>> line begin <<<------");
		
		String time = line.substring(11,19);
		System.out.println("time:" + time);
		
		String json = line.substring(24);
		System.out.println("json:" + json);
		
		JSONObject jsonObject = JSONObject.parseObject(json);
		String pamramsString = jsonObject.getString("PAMRAMS");
		System.out.println("pamramsString:" + pamramsString);
		String pamramsJson = pamramsString.replaceAll("\\[", "");
		pamramsJson = pamramsJson.replaceAll("\\]", "");
		System.out.println("pamramsJson:" + pamramsJson);
		
		JSONObject pamramsObject = JSONObject.parseObject(pamramsJson);
		
		Long siteId = pamramsObject.getLongValue("jz");
		Long redPacketId = pamramsObject.getLongValue("hb");
		String phone = pamramsObject.getString("dh");
		String fbj = pamramsObject.getString("fbj");
		String zmtmu = pamramsObject.getString("zmtmu");
		
		JSONObject logMapObject = jsonObject.getJSONObject("LOG_MAP");
		
		Integer amount = logMapObject.getInteger("TRANSFER_AMOUT");
		
		String positonJson = logMapObject.getString("POSITION");
		JSONObject positonObject = JSONObject.parseObject(positonJson);
		String formatted_address = positonObject.getString("formatted_address");
		System.out.println("formatted_address:"+formatted_address);
		
		WeChatUserInfo weChatUserInfo = logMapObject.getObject("USERINFO", WeChatUserInfo.class);
		System.out.println("weChatUserInfo:" + JSONObject.toJSONString(weChatUserInfo));
		
		String postXml = logMapObject.getString("TRANSFER_POST_PARAMS");
		Map<String, String> xmlMap = XMLUtil.doXMLParse(postXml);
		System.out.println("xmlMap:" + JSONObject.toJSONString(xmlMap));
		
		String orderId = xmlMap.get("partner_trade_no");
		String openid = xmlMap.get("openid");
		
		// 创建红包领取记录
		RedPacketSendRecord redPacketSendRecord = new RedPacketSendRecord();
		redPacketSendRecord.setOrderId(orderId);
		redPacketSendRecord.setOpenId(weChatUserInfo.getOpenid());
		redPacketSendRecord.setAmount(amount);
		redPacketSendRecord.setDate(DF.parse("2019-03-30"));
		redPacketSendRecord.setStatus(0);
		redPacketSendRecord.setReason("denied to user dsp_bidserver@11.192.74.67");
		redPacketSendRecord.setNickName(weChatUserInfo.getNickname());
		redPacketSendRecord.setSex(weChatUserInfo.getSex());
		redPacketSendRecord.setHeadImgUrl(weChatUserInfo.getHeadimgurl());
		redPacketSendRecord.setExt(JSONObject.toJSONString(weChatUserInfo));
		redPacketSendRecord.setSiteId(siteId);
		redPacketSendRecord.setCashPackageId(redPacketId);
		redPacketSendRecord.setMediaId(0);
		redPacketSendRecord.setPhone(phone);
		redPacketSendRecord.setParentOpenId(fbj);
		redPacketSendRecord.setUserId(0L);
		redPacketSendRecord.setArea(formatted_address);
		redPacketSendRecord.setUrl(zmtmu);
		redPacketSendRecord.setCreateTime(SDF.parse("2019-03-30 " + time));
		
		redPacketSendRecordMapper.createRedPacketSendRecord(redPacketSendRecord);
		
		System.out.println("------>>> line end <<<------");
	}
	
	
	
	

}
