package com.yqh.wechat.controller;

import java.util.UUID;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yqh.wechat.rabbitmq.config.RabbitMqConfig;


@RestController
@RequestMapping("/mq")
public class RabbltMqController {

	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@RequestMapping("/send1")
	public void send1(){
		System.out.println("----------------------------------------------------------------------------------------------");
		String uuid = UUID.randomUUID().toString();
		System.out.println("[ RabbltMqController send1 ] >>> " + uuid);
		CorrelationData correlationId = new CorrelationData(uuid);
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE, RabbitMqConfig.ROUTINGKEY_A, uuid, correlationId);
	}
	
	
}
